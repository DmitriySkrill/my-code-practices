<?php
$fileName = "hitsCount.txt";
if (file_exists ($fileName)) {
    $hitsCounter = (int)file_get_contents($fileName);
} else {
    $hitsCounter = 0;
}

$hitsCounter++;
file_put_contents($fileName, $hitsCounter);

echo 'Страница была загружена ' . $hitsCounter . ' раз(а). Текущее время ' . date("H:i");