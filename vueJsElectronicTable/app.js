const elTabCellTemplate = (value = '', spanVisibility = true, inputVisibility = false) => ({
    value,
    spanVisibility,
    inputVisibility
})

Vue.directive('focus', {
    inserted: function (chosenInput) {
        chosenInput.focus()
    }
})

let elTable = []

function returnElTableSizeFromLocalStorage () {
    let elTableSize = {
        maxRowNumber: 0,
        maxCellNumber: 0
    }
    Object.keys(localStorage).forEach(function (key) {
        let keysArr = splitString(key, '.')
        if (parseInt(keysArr[0]) >= elTableSize.maxRowNumber) {
            elTableSize.maxRowNumber = parseInt(keysArr[0])
        }
        if (parseInt(keysArr[1]) >= elTableSize.maxCellNumber) {
            elTableSize.maxCellNumber = parseInt(keysArr[1])
        }
    })
    return elTableSize
}

function createElTable () {
    let elTableSize = returnElTableSizeFromLocalStorage()
    for (let i = 0; i <= elTableSize.maxRowNumber; i++) {
        let newElTabRow = []
        for (let j = 0; j <= elTableSize.maxCellNumber; j++) {
            newElTabRow.push(elTabCellTemplate())
        }
        elTable.push(newElTabRow)
    }
}

function fillElTableFromLocalStorage () {
    Object.keys(localStorage).forEach(function (key) {
        let keysArr = splitString(key, '.')
        elTable[parseInt(keysArr[0])][parseInt(keysArr[1])] = elTabCellTemplate(localStorage.getItem(key))
    })
}

function splitString (stringToSplit, separator) {
    let arrayOfStrings = stringToSplit.split(separator)
    return arrayOfStrings
}

function addDataToLocalStorage (elTabRowIndex, elTabCellIndex, value) {
    let key = String
    key = elTabRowIndex + '.' + elTabCellIndex
    if (value === undefined) {
        value = ''
    }
    localStorage.setItem(key, value)
}

createElTable()
fillElTableFromLocalStorage()

let ElTabRowComponent = {
    template: '#el-tab-row-template',
    props: {
        elTabRow: Array,
        elTabRowIndex: Number
    },
    methods: {
        showInput (elTabCell) {
            elTabCell.spanVisibility = false
            elTabCell.inputVisibility = true

        },
        hideInput (elTabRowIndex, elTabCellIndex, elTabCell) {
            elTabCell.spanVisibility = true
            elTabCell.inputVisibility = false
            addDataToLocalStorage (elTabRowIndex, elTabCellIndex, elTabCell.value)
        },

    }
}

new Vue({
    el: '#app',

    data: {
        elTable: elTable
    },
    components: {
        'el-tab-row': ElTabRowComponent
    },
    methods: {
        addElTabColumn () {
            let rootThis = this
            this.elTable.forEach(function (elTabRow, elTabRowIndex) {
                elTabRow.push(elTabCellTemplate())
                addDataToLocalStorage(elTabRowIndex, (elTabRow.length - 1), rootThis.LastChild(elTabRow).value)
            })
        },
        delElTabColumn () {
            let key = String
            if (this.elTable[0].length > 1 && this.isDataInElTabCell() === false) {
                this.elTable.forEach(function (elTabRow, elTabRowIndex) {
                    elTabRow.pop()
                    key = elTabRowIndex + '.' + elTabRow.length
                    localStorage.removeItem(key)
                })
            }
        },
        addElTabRow () {
            let rootThis = this
            this.elTable.push(this.createElTabRow(this.elTable[0].length))
            this.LastChild(this.elTable).forEach(function (elTabCell, elTabCellIndex) {
                addDataToLocalStorage((rootThis.elTable.length - 1), elTabCellIndex, elTabCell.value)
            })
        },
        delElTabRow () {
            let rootThis = this
            let key = String
            if (rootThis.elTable.length > 1 && this.isDataInElTabRow(rootThis.LastChild(elTable)) === false) {
                rootThis.elTable.pop()
                rootThis.LastChild(rootThis.elTable).forEach(function (elTabCell, elTabCellIndex) {
                    key = (rootThis.elTable.length) + '.' + elTabCellIndex
                    localStorage.removeItem(key)
                })
            }
        },
        createElTabRow (length) {
            let newElTabRow = []
            for (let i = 0; i < length; i++) {
                newElTabRow.push(elTabCellTemplate())
            }
            return newElTabRow
        },
        closeAllInput () {
            let rootThis = this
            this.elTable.forEach(function (elTabRow, elTabRowIndex) {
                elTabRow.forEach(function (elTabCell, elTabCellIndex) {
                    elTabCell.spanVisibility = true
                    elTabCell.inputVisibility = false
                    addDataToLocalStorage(elTabRowIndex, elTabCellIndex, elTabCell.value)
                })
            })
        },
        isDataInElTabRow (row) {
            let answer = false
            row.forEach(function (cell) {
                if (cell.value) {
                    answer = true
                }
            })
            return answer
        },
        isDataInElTabCell () {
            let rootThis = this
            let answer = false
            this.elTable.forEach(function (elTabRow) {
                if (rootThis.LastChild(elTabRow).value) {
                    answer = true
                }
            })
            return answer
        },
        LastChild (object) {
            return object[object.length - 1]
        },

    },
    computed: {},
})

