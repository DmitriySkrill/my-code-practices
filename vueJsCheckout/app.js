ymaps.ready(init);
var yandexMap;

function init() {
    yandexMap = new ymaps.Map("map", {
        center: [54.1937, 37.6197],
        zoom: 11
    }, {
        balloonMaxWidth: 200,
        searchControlProvider: 'yandex#search'
    });

    // Обработка события, возникающего при щелчке
    // левой кнопкой мыши в любой точке карты.
    // При возникновении такого события откроем балун.
    yandexMap.events.add('click', function (e) {
        if (!yandexMap.balloon.isOpen()) {
            var coords = e.get('coords');
            yandexMap.balloon.open(coords, {
                contentHeader: 'Координаты точки!',
                contentBody: '<p id="mapBalloonCheck">' + [
                    coords[0].toPrecision(6),
                    coords[1].toPrecision(6)
                ].join(', ') + '</p>',
                contentFooter: ''
            });
        } else {
            yandexMap.balloon.close();
        }
    });

    // Скрываем хинт при открытии балуна.
    yandexMap.events.add('balloonopen', function (e) {
        yandexMap.hint.close();
    });
}

const app = new Vue({
    el: '#app',
    data: {
        errors: [],
        name: null,
        tel: null,
        email: null,
        orderIsProcessed: false
    },
    methods: {
        checkForm: function (e) {
            this.errors = [];
            if (!this.name) {
                this.errors.push('Не заполнено поле ФИО');
            }
            if (!this.tel) {
                this.errors.push('Не заполнено поле телефон');
            } else if (this.validItem(this.tel, /[^0-9,]/)) {
                this.errors.push('В поле телефон присутсвтуют символы отличные от числовых');
            }
            if (!this.validItem(this.email, /@/)) {
                this.errors.push('В поле email отсутствует знак @');
            }
            if (document.getElementById('mapBalloonCheck')) {

            } else {
                this.errors.push('Не отмечен адрес доставки');
            }
            if (!this.errors.length) {
                this.orderIsProcessed = true;
                return true;
            } else {
                this.orderIsProcessed = false;
            }
            e.preventDefault();
        },
        validItem: function (itemToCheck, re) {
            return re.test(itemToCheck);
        }
    }
})