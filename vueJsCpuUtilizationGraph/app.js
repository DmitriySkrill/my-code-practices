let ctx = document.getElementById('cpuLoadChart').getContext('2d')
let cpuLoadChart = new Chart(ctx, {
    type: 'line',
    data: {
        datasets: [{
            label: 'График загруженности процессора',
        }]
    },
    options: {}
})
const interval = setInterval(() => {}, 1000);
let cpuLoad = new Vue({
    el: '#cpuLoad',
    data: {
        cpuLoads: [],
        requestCounter: 0,
        errorCounter: 0,
        interval: ''
    },
    methods: {
        setIntervalGetCpuLoads () {
            this.interval = setInterval(
                this.getCpuLoad,
                1000)
        },
        getCpuLoad () {
            this.$http.get('http://exercise.develop.maximaster.ru/service/cpu/')
                .then(response => {
                    this.cpuLoads.push(this.createCpuLoads(response.body))
                }, response => {
                    if (this.cpuLoads.length === 0) {
                        this.cpuLoads.push(this.createCpuLoads(0))
                    } else {
                        this.cpuLoads.push(this.createCpuLoads(this.returnLastCpuLoad()))
                    }
                    this.errorCounter++
                })
                .finally(() => {
                    this.requestCounter++
                    this.updateChart(this.cpuLoads)
                })
        },
        returnLastCpuLoad () {
            return this.cpuLoads[this.cpuLoads.length - 1].y
        },
        createCpuLoads (load) {
            let newObj = {}
            newObj['x'] = moment(new Date())
            newObj['y'] = load
            return newObj
        },
        returnErrorsPercent () {
            return Math.round(this.errorCounter / this.requestCounter * 100)
        },
        updateChart (cpuLoads) {
            let data = [], labels = []
            cpuLoads.forEach(function (cpuLoad) {
                data.push(cpuLoad.y)
                labels.push(cpuLoad.x.format('H:mm:ss'))
            })
            cpuLoadChart.data.datasets[0].data = data
            cpuLoadChart.data.labels = labels
            cpuLoadChart.update()
        }
    },
    beforeDestroy () {
        clearInterval(this.interval)
    },
    mounted () {
        this.setIntervalGetCpuLoads()
    }
})

