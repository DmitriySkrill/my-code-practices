<?php
include 'function.php';
include 'citiesList.php';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Калькулятор доставки</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="style.css">
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col">
            <h1>Калькулятор доставки</h1>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <div class="alert alert-danger mb-3" id="errorInput">
            </div>
            <div class="alert alert-danger mb-3" id="errorBlock">
                <p>Цена:<span class="wwText" id="errorPrice"> </span></p>
                <p>Ответ:<span class="wwText" id="errorMessage"> </span></p>
            </div>
            <div class="alert alert-success" id="successBlock" role="alert">
                <p>Цена:<span class="wwText" id="successPrice"> </span></p>
                <p>Ответ:<span class="wwText" id="successMessage"> </span></p>
            </div>
        </div>
    </div>
    <div class="row mb-5">
        <div class="col">
            <form action="http://exercise.develop.maximaster.ru/service/delivery/">
                <div class="form-group">
                    <? createSelectInput($cities); ?>
                </div>
                <div class="form-group">
                    <input type="number" class="form-control form-control-sm" id="weight" name="weight"
                           placeholder="Вес, кг" required>
                </div>
                <button type="submit" class="btn btn-secondary btn-sm" id="sendToDbSubmit">Отправить submit'ом</button>
                <button type="button" class="btn btn-secondary btn-sm" id="sendToDbAjax">Отправить Ajax</button>
            </form>
        </div>
    </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="ajax.js"></script>
</body>
</html>