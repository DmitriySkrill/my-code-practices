<?php
function createSelectInput($options)
{
    ?>
    <select id="city" class="form-control form-control-sm" name="city">
        <? foreach ($options as $option) { ?>
            <option <?= ($option === 'Москва') ? 'Selected' : '' ?>><?= $option ?></option>
        <? } ?>
    </select>
<?php } ?>