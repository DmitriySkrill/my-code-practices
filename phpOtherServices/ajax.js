$('#sendToDbAjax').click(function () {

    let city = $('#city').val(),
        weight = parseInt($('#weight').val()),
        errorInput = $('#errorInput'),
        successBlock = $('#successBlock'),
        errorBlock = $('#errorBlock'),
        successPrice = $('#successPrice'),
        successMessage = $('#successMessage'),
        errorPrice = $('#errorPrice'),
        errorMessage = $('#errorMessage')

    //Очистка значений и скрытие информационных блоков
    successBlock.hide()
    successPrice.text('')
    successMessage.text('')

    errorBlock.hide()
    errorPrice.text('')
    errorMessage.text('')
    //...Очистка значений и скрытие информационных блоков

    if (isNaN(weight)) {
        errorInput.show()
        errorInput.text('Вес должен быть введен и введен целочисленным')
        return
    }

    $.ajax({
        url: 'http://exercise.develop.maximaster.ru/service/delivery/',
        type: 'GET',
        cache: false,
        data: {
            'city': city,
            'weight': weight
        },
        dataType: 'json',
        success: function (data) {
            if (data['status'] === 'OK') {
                successBlock.show()
                successPrice.text(data['price'])
                successMessage.text(data['message'])
            } else {
                errorBlock.show()
                errorPrice.text(data['price'])
                errorMessage.text(data['message'])
            }
        }
    })

})