const app = new Vue({
    el: "#app",
    data: {
        priceFrom: 0,
        priceTo: 0,
        products: [{}],
        filteredProducts: [{}],
    },
    mounted() {
        this.$http.get('http://exercise.develop.maximaster.ru/service/products/').then(response => {
            this.products = response.body;
            this.filterProducts();
        }, response => {
        });
    },
    methods: {
        normalizePrice: function (elem) {
            switch (elem) {
                case '':
                case null:
                case true:
                case false:
                case undefined:
                case NaN:
                    elem = 0;
                    break;
                default:
                    break;
            }
            if (elem < 0) {
                elem = 0
            }
            elem = parseInt(elem);
            return elem;
        },
        filterProducts() {
            this.filteredProducts = this.products.filter(product => {

                //т.к. все значения в input лежат в виде текста, то поставим фильтр для преобразования к числу
                let normalizedPriceFrom = this.normalizePrice(this.priceFrom);
                let normalizedPriceTo = this.normalizePrice(this.priceTo);

                if ((normalizedPriceFrom <= product.price && normalizedPriceTo >= product.price) || (normalizedPriceTo === 0 && normalizedPriceFrom === 0)) {
                    return true;
                }
            });
        }
    }
})

