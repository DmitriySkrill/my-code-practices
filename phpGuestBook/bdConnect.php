<?php
/*
 * $host, $db, $user, $pass - переменные задаются в файле `bdDataToConnect.php`
 */
include 'bdDataToConnect.php';
$dsn = "mysql:host=$host;dbname=$db;";

try {

    $pdo = new PDO($dsn, $user, $pass);

} catch (PDOException $Exception) {

    //throw new MyDatabaseException($Exception->getMessage(), (int)$Exception->getCode()); - не понимаю как то дальше использовать
    $dataBaseConnectStatus['success'] = false;
    $dataBaseConnectStatus['message'] = 'Ошибка подключения к базе данных';
    $dataBaseConnectStatus['serverMessage']['Message'] = $Exception->getMessage();
    $dataBaseConnectStatus['serverMessage']['Code'] = $Exception->getCode();

}