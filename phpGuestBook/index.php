<?php
$dataBaseConnectStatus = [
    'success' => true,
    'message' => '',
    'serverMessage' => []
];
//Тестовое подключение к базе данных
include 'bdConnect.php';
$pdo = null; // закрываем тестовое соединение с бд
include 'function.php';
?>
<!DOCTYPE html>
<html lang="ru">

<head>

    <meta charset="UTF-8">
    <title>Гостевая книга</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="style.css">

</head>

<body>

<div class="container">

    <?php if ($dataBaseConnectStatus['success']) {

        include 'content.php';

    } else { ?>

        <div class="alert alert-danger mt-5" role="alert">

            <?php
            echo $dataBaseConnectStatus['message'] . '<br>' . '<hr>';
            foreach ($dataBaseConnectStatus['serverMessage'] as $key => $value) {
                ?>
                <h6><?= $key ?>:</h6>
                <p><?= $value ?></p>
            <?php } ?>

        </div>

    <?php } ?>

</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

</body>
</html>
