<?php
function createBookEntries($date, $userName, $text)
{
    ?>
    <div class="note mt-3">
        <div class="row my-2">
            <div class="col-6">
                <span class="px-2 message"><?= date('d.m.Y H:i', $date); ?></span>
            </div>
            <div class="col-6">
                <span class="float-right px-2 message"><?= $userName; ?></span>
            </div>
        </div>
        <div class="row my-2">
            <div class="col">
                <span class="px-2 message"><?= $text; ?></span>
            </div>
        </div>
    </div>
    <?php
}