<div class="row">
    <div class="col">
        <h1>Гостевая книга</h1>
    </div>
</div>
<div class="row">
    <div class="col">
        <? include 'guestBookEntries.php' ?>
    </div>
</div>
<hr>
<div class="row mb-5">
    <div class="col">
        <form action="core.php" method="post">
            <div class="form-group">
                <input type="text" class="form-control" name="userName" placeholder="Имя">
            </div>
            <div class="form-group">
                <textarea class="form-control" rows="3" name="text" placeholder="Ваше сообщение" required></textarea>
            </div>
            <button type="submit" class="btn btn-secondary btn-sm" id="sendToDb">Отправить</button>
        </form>
    </div>
</div>