<?php
$answer = [
    'success' => true,
    'message' => ''
];

if (strlen($_POST['text']) > 0) {

    $text = trim(filter_var($_POST['text'], FILTER_SANITIZE_STRING));

    if (strlen($_POST['userName']) > 0) {
        $userName = trim(filter_var($_POST['userName'], FILTER_SANITIZE_STRING));
    } else {
        $userName = 'Анонимно';
    }

    $date = date('U');

    include 'bdConnect.php';

    $sql = 'INSERT INTO notes(userName, text, date) VALUES(:userName, :text, :date)';
    $query = $pdo->prepare($sql);
    $queryResult = $query->execute(['userName' => $userName, 'text' => $text, 'date' => $date]);

    $pdo = null;

    if ($queryResult === false) {

        $answer['success'] = $queryResult;
        $answer['message'] = 'Произошла ошибка при записи в базу данных';

    }

} else {

    $answer['success'] = false;
    $answer['message'] = 'Введите текст сообщения в соответствующее поле';

}

if ($answer['success'] === false) {

    echo '<p>' . $answer['message'] . '</p>';
    echo '<a href="index.php">Вернуться к гостевой книге</a>';

} else {

    header('Location: index.php');

}