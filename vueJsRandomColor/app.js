let ColorBlockComponent = {
    template: '#color-block-template',
    props: {
        blockBackgroundColor:  String,
        blockWidth: Number,
        blockHeight: Number,
    }
};

var app = new Vue({
    el: '#app',
    data: function () {
        return{
            blockBackgroundColor:  this.generateBlockColor(),
            blockWidth: 100,
            blockHeight: 100,
        }
    },
    methods: {
        changeBlockColor: function () {
            this.blockBackgroundColor =  this.generateBlockColor()
        },
        generateBlockColor(){
            return '#' + (Math.random() * 0xFFFFFF << 0).toString(16)
        }
    },
    components: {
        'color-block': ColorBlockComponent
    }
})

